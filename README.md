## VOOT

General distributed task runner

```js
var Task = require('voot').Task;
var sender = new Task();
var count = 0;

sender.dispatch({count:++count, pid: process.pid },function( ack ){
	console.log('message %s acked at %s', ack.count, ack.time)	
});

var handler = new Task({
	handle:function( msg, ack ){

		console.log("I got a message", msg );
		
		ack({
			count:msg.count
		  , time:( +new Date() ) 
		});
	}
})
```
