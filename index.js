/*jshint laxcomma: true, smarttabs: true*/
/*globals module,process,require,exports,__dirname,__filename */
'use strict';
/**
 * Distributed task management for the megadoomer platform
 * @module megadoomer-tasks
 * @author 
 * @since 0.1.0
 * @requires megadoomer-tasks/events 
 * @requires megadoomer-tasks/commands 
 * @requires megadoomer-tasks/models 
 * @requires megadoomer-tasks/lib 
 */


module.exports = require('./events');



// models
module.exports.models = require('./models')

