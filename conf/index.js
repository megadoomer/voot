/*jshint laxcomma: true, smarttabs: true, node: true*/
'use strict';
/**
 * Configuration options for megadoomer-tasks
 * @module module:voot/conf
 * @author Eric Satterwhite
 * @since 0.1.0
 */

exports.voot = {
		rabbitmq:{
			connection:{
				"server": "localhost",
				"port": 5672,
				"vhost": '%2f'
			}
			,task:{
				base:{
					driver: 'rabbitmq',
					exchange: "megadoomer.tasks.exchange",
					queue: "megadoomer.task.base.queue",
					key: "megadoomer.task.base.#",
					limit: 1,
					type: "megadoomer.task.base.message",
				}
			}
		}
};
