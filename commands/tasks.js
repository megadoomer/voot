/*jshint laxcomma: true, smarttabs: true*/
/*globals module,process,require,exports,__dirname,__filename */
'use strict';
/**
 * Default command for the megadoomer-tasks package
 * @module megadoomer-tasks/commands/tasks
 * @author 
 * @since 0.0.1
 * @requires seeli
 * @requires util
 */

var cli = require( 'seeli' )
  , util = require( 'util' )
  ;

module.exports = new cli.Command({
	description:"Default command for megadoomer-tasks package"
	,usage:[
		cli.bold('Usage: ') + 'megadoomer tasks --help'
	  , cli.bold('Usage: ') + 'megadoomer tasks --no-color'
	  , cli.bold('Usage: ') + 'megadoomer tasks -i'
	]

	,flags:{
		'default':{
			type: Boolean
			,description:"Enable the default"
			,default:true
			,required:false
		}
	}
	/**
	 * This does something
	 * @param {String|null} directive a directive passed in from the cli
	 * @param {Object} data the options collected from the cli input
	 * @param {Function} done the callback function that must be called when this command has finished
	 * @returns something
	 **/
	,run: function( cmd, data, done){

		done(/* error */ null, /* output */ 'success')
	}
});
