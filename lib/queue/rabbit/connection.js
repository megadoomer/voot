/*jshint laxcomma: true, smarttabs: true*/
/*globals module,process,require,exports,__dirname,__filename */
'use strict';
/**
 * Connection class to interface with Rabbit
 * @module voot/lib/queue/rabbit/connection
 * @author Eric satterwhite
 * @since 0.1.0
 * @requires util
 * @requires rabbus
 * @requires wascally
 * @requires gaz/lib/class
 * @requires gaz/lib/class/options
 * @requires gaz/lib/class/parent
 */

var util    = require("util")
  , Rabbus  = require("rabbus")
  , Rabbit  = require('wascally')
  , conf    = require('keef')
  , debug   = require('debug')('voot:queue:rabbit:connection')
  , Class   = require('gaz/class')
  , Options = require('gaz/class/options')
  , Parent  = require('gaz/class/parent')


Rabbit.configure( conf.get('voot:rabbitmq') ).done( function( err ){
	if( err ){
		throw err;
	}
})
Rabbit.on('connected', function( ){
	debug("connected to rabbitmq", conf.get('voot:rabbitmq') )
})
Rabbit.on('closed', debug.bind(console, 'rabbitmq connection: closed') )
Rabbit.on('failed', debug.bind(console, 'rabbitmq connection: failed') )
module.exports = Rabbit;
