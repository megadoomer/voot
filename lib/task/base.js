/*jshint laxcomma: true, smarttabs: true*/
/*globals module,process,require,exports,__dirname,__filename */
'use strict';
/**
 * base.js
 * @module voot/lib/task/base
 * @author Eric Satterwhite
 * @since 0.1.0
 * @requires util
 * @requires crypto
 * @requires rabbus
 * @requires wascally
 * @requires voot/lib/queue/rabbit/connection
 * @requires gaz/class
 * @requires gaz/class/options
 * @requires gaz/class/parent
 */

var util       = require("util")
  , events     = require('events')
  , crypto     = require('crypto')
  , Rabbus     = require('rabbus')
  , Rabbit     = require('wascally')
  , Class      = require('gaz/class')
  , Options    = require('gaz/class/options')
  , Parent     = require('gaz/class/parent')
  , connection = require("../queue/rabbit/connection")
  , Task
/**
 * @constructor
 * @alias module:voot/lib/task/base
 * @param {object} options Task configuration options
 * @param {String} [options.exchange="voot.tasks.exchange"]
 * @param {String} [options.queue="voot.task.base.queue"]
 * @param {String} [options.key="voot.task.base.#"]
 * @param {Number} [options.limit=1]
 * @param {String} [options.type="voot.task.base.message"]
 * @param {?String} [options.handle=null]
 * @example
var Task = require('voot').Task;
var sender = new Task();
var count = 0;


var handler = new Task({
	handle:function( msg, ack ){

		console.log("I got a message", msg );
		
		ack({
			count:msg.count
		  , time:( +new Date() ) 
		});
	}
});

setInterval(function(){
	sender.dispatch({count:++count, pid: process.pid },function( ack ){
		console.log('message %s acked at %s', ack.count, ack.time)	
	});
}, 500);
 */
Task = new Class({
	inherits:events.EventEmitter
	,mixin: [Options, Parent] 
	,options:{
		exchange: "voot.tasks.exchange",
		queue: "voot.task.base.queue",
		key: "voot.task.base.#",
		limit: 1,
		type: "voot.task.base.message",
		handle:null
	}

	,constructor: function( options ){
		this.setOptions( options );
		this.consumer = this.toReceiver()
		this.consumer && this.consumer.handle( this.options.handle );
		this.sender = this.toSender()
	}

	/**
	 * Sends a message
	 * @chainable
	 * @method module:voot/lib/task/base#dispatch
	 * @param {Mixed} msg a message to dispatch.
	 * @param {Function} callback A function to be called when the message is ackowledged
	 **/
	,dispatch: function dispatch( msg, cb ){
		this.sender.request( msg || {}, cb );
		return this;
	}

	/**
	 * responsible for generating a sender instance specific to the backend.
	 * @private
	 * @method module:voot/lib/task/base#toSender
	 * @return {?Requester} A requester instance, if no handle has been specified
	 **/
	,toSender: function toSender(){
		return !this.options.handle && new Rabbus.Requester( connection,{
			exchange    : this.options.exchange
		  , routingKey  : this.options.key
		  , messageType : this.options.type
		})
	}

	/**
	 * Responsible for creating a responder instance if a handle function is defined 
	 * @method module:voot/lib/task/base#toReceiver
	 * @return {?Responder} A responder instance
	 **/
	,toReceiver: function toReceiver(){
		return this.options.handle &&  new Rabbus.Responder(connection,{
			exchange    : this.options.exchange
		  , queue       : this.options.queue
		  , routingKey  : this.options.key
		  , limit       : this.options.limit
		  , messageType : this.options.type
		});
	}

	/**
	 * Disconnects from the queue backend
	 * @method module:voot/lib/task/base#disconnect
	 * @param {TYPE} NAME ...
	 * @param {TYPE} NAME ...
	 * @return {TYPE} DESCRIPTION
	 **/
	,disconnect: function disconnect( cb ){
		return connection.closeAll().then( cb );
	}
});

module.exports = Task;
