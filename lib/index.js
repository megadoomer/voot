/*jshint node:true, laxcomma: true, smarttabs: true*/
'use strict';
/**
 * THIS MODULE DOES THINGS
 * @module MODULE NAME
 * @author YOUR NAME GOES HERE
 * @since 0.1.0
 * @requires moduleA
 * @requires moduleB
 * @requires moduleC
 */


var moduleA = require( 'moduleA' )  // this is moduleA
  , moduleB = require( 'moduleB' )  // this is moduleB
  , moduleC = require( 'moduleC' )  // this is moduleC
  , AClass                          // Primary classs
  ;

/**
 * CLASS DESCRIPTION
 * @class module:path/to/file.Class
 * @param {Object} options the options thing
 * @mixes module:path/to/some/module.Class
 * @borrows module:path/to/some/module.Class
 * @throws {SomeError}
 * @fires shake
*/
AClass = new Class(/** @lends module:path/to/file.Class.prototype */{
	/**
	 * DESCRIPTION
	 * @param {Number} value A value
	 * @param {Function} [callback] an optional callback
	 * @throws {Error} Thrown when it does someting bad
	 * @returns {Object} obj The return object
	 */
	method: function(){
	    var n = this.options.next();
	    /**
	     * @name moduleName.Class#shake
	     * @event
	     * @param {Event} e
	     * @param {Boolean} [e.withIce=false]
	     */
	    this.emit( 'shake', n );
	    return n;
	},
});

module.exports = AClass