/*jshint laxcomma: true, smarttabs: true*/
/*globals module,process,require,exports,__dirname,__filename */
'use strict';
/**
 * Model classes for the megadoomer-tasks package
 * @module megadoomer-tasks/models
 * @author 
 * @since 0.0.1
 * @requires mongoose
 * @requires megadoomer-core/lib/db
 */

var mongoose = require( 'mongoose' )
  , connection = require("megadoomer/lib/db").connection
  , _Schema
  , Model
  ;


_Schema = new mongoose.Schema({
	field:String
});


/**
 * Finds things of a similar type
 * @param {Function} callback Callback function to be called when the query has finished
 **/
_Schema.methods.byField = function( cb ){
	return this.model('Model').find({field: this.field}, cb)
}

/**
 * X Model for the megadoomertasks package
 * @class module:_models.js.Thing
 * @param {Object} data mapping of {fieldname: value } pairs to poulate the model with
 */
Model = connection.commerce.model('Model', _Schema);

Model.schema.path('field').validate( function( value ){
	return value != null
}, 'Invalid Field');

module.exports = Model
